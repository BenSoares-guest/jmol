#!/bin/sh
# Fully automated Jmol source distribution tarball, that adheres to FOSS and
# general software-engineering best practise. CURRENTLY INCOMPLETE.
#
# TODO: I am trying to persuade upstream to take over maintainenance of this
# script or provide something equivalent. See:
#    https://sourceforge.net/p/jmol/bugs/587/?page=1
#
# TODO: THIS SCRIPT IS CURRENTLY INCOMPLETE. We need to figure out how to
# generate JSmol/j2s/java and JSmol/srcjs. See:
#    https://sourceforge.net/p/jmol/bugs/589/
# Afterwards, we need to (1) ignore_and_remove these paths, and (2) add the
# correct commands to regenerate it, to the Makefile further below.
#
set -e
VERSION="14.6.4+2016.11.05"

ignore_and_remove() {
	eval rm -rf "$1"
	# Add what we just ignored to .gitignore. This is just for demonstration
	# purposes - you should set the equivalent svn:ignore property in your SVN.
	echo "/$1" >> .gitignore
}

pkgdir="jmol_${VERSION}_full-src"
if test "$1" = print-name; then echo "$pkgdir"; exit; fi
rm -rf "$pkgdir"
mkdir -p "$pkgdir"

echo >&2 "Building directory structure in: $pkgdir"
# Work inside that directory
set -x
( cd "$pkgdir"

################################################################################
# Check out source code from SVN
################################################################################

svn="svn --config-option config:miscellany:use-commit-times=yes"

# These SVN revision numbers need to be updated manually for each new release.
# However it is much easier for the upstream developers to do this, than for
# distro packagers to try to reverse-engineer it - hence why I filed bug #587.

# https://sourceforge.net/p/jmol/code/HEAD/tree/
$svn checkout svn://svn.code.sf.net/p/jmol/code/branches/v14_6/Jmol@21289
# https://sourceforge.net/p/jspecview/svn/HEAD/tree/dev2/
$svn checkout svn://svn.code.sf.net/p/jspecview/svn/dev2/JSpecView@1735
$svn checkout svn://svn.code.sf.net/p/jspecview/svn/dev2/JSpecViewLib@1735
# https://sourceforge.net/p/jsmol/code/HEAD/tree/trunk/
$svn checkout svn://svn.code.sf.net/p/jsmol/code/trunk@935 JSmol

# Remove SVN directories
rm -rf */.svn

lastdate="$(find . -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -b1-10)"

# Now we remove many files so the working directory is clean. Ideally, upstream
# would simply remove these from SVN too, so that this script doesn't have to
# be updated with the correct things to remove, every single release. However,
# they don't seem to understand the benefits of this practise.

################################################################################
# Remove auto-generated files
################################################################################

# This makes it easier to ensure no generated files are stale or from older
# versions of the source code. It also makes it easier for others to verify
# that the source code is actually free and open source. But upstream doesn't
# seem interested in trying to understand these points.

echo "# Ignore auto-generated files" >> .gitignore
ignore_and_remove 'Jmol/appletweb/*.zip'
ignore_and_remove Jmol/jars/JSpecView.jar
ignore_and_remove Jmol/srcjsv/jspecview/
ignore_and_remove 'JSmol/dist/*.zip'
ignore_and_remove JSmol/jars/JSpecView.jar
ignore_and_remove 'JSmol/jnlp/*.jar'
ignore_and_remove JSmol/srcjs/javajs/
ignore_and_remove JSpecView/build/
ignore_and_remove 'JSpecView/dist/*.zip'
ignore_and_remove JSpecView/src/org/jmol/
ignore_and_remove JSpecViewLib/src/javajs/
ignore_and_remove JSpecViewLib/src/org/jmol/

ignore_and_remove JSpecView/doc/
sed -i -e 's/\r$//g' JSpecView/build.xml # convert CRLF to LF
patch -p1 <<'eof'
diff --git a/JSpecView/build.xml b/JSpecView/build.xml
index 13d11b7..880e3d3 100644
--- a/JSpecView/build.xml
+++ b/JSpecView/build.xml
@@ -288,6 +289,17 @@
        </java>
    	</target>
 
+    <target name="javadoc" description="Generation of Javadoc">
+      <mkdir dir="doc"/>
+      <javadoc destdir="doc" sourcepath="src"
+               private="true" encoding="UTF8"
+               packagenames="jspecview.*"
+               excludepackagenames="org.jmol.*">
+        <classpath refid="jspecview.classpath" />
+        <link href="http://download.oracle.com/javase/7/docs/api/" />
+      </javadoc>
+    </target>
+
 	<target name="clean" >
        <delete quiet="true">
          <fileset dir ="${build.dir}" includes= "**/*.class" />
eof
sed -i -e 's/$/\r/g' JSpecView/build.xml # convert LF back to CRLF

#ignore_and_remove JSmol/lib/jsme/jsme/
# Peter Ertl <peter.ertl@gmail.com> to Ximin Luo <infinity0@pwned.gg>
#   "The underlying Java source is unfortunately due to various legacy legal
#    reasons not available."
# However, I leave it in this tarball for documentation purposes; there is no
# other way to produce these files. We'll remove them from the Debian tarball.

################################################################################
# Remove unused and obsolete files
################################################################################

# These files don't seem to be used, but were used in an older version. We
# don't want them in the source tarball since they waste time for new reviewers
# who have to spend time figuring out that they're not used. Upstream should
# simply delete them from SVN; if they need them in the future, they can check
# them out again from a previous revision.

rm -rf Jmol/appletweb/old/
rm -rf Jmol/packaging/
rm -rf Jmol/plugin-jars/
rm -rf Jmol/src/com/sparshui/gestures/unused_gestures.zip
rm -rf Jmol/unused/
rm -rf JSmol/jars/Acme.jar
rm -rf JSmol/jars/commons-cli-1.0.jar
rm -rf JSmol/jars/gnujaxp.jar
rm -rf JSmol/jars/gnujaxp-onlysax.jar
rm -rf JSmol/jars/itext-1.4.5.jar
rm -rf JSmol/jars/naga-2_1-r42.jar
rm -rf JSmol/lib/jsme0/
rm -rf JSmol/lib/jme/
rm -rf JSmol/old/
rm -rf JSmol/unused/
rm -rf JSpecView/unused/

# This is in the wrong place, Jmol needs it not JSmol
mv JSmol/jars/saxon.jar Jmol/jars/saxon.jar

################################################################################
# Add a Makefile
################################################################################

# This is a script which will automatically build everything.
# It recreates the instructions in Jmol/build.README.txt, but fully-automated.
# This allows for much larger-scale engineering than clicking on different
# buttons in Eclipse many many times manually.
cat >Makefile <<'eof'
PRIVATE_FILE ?= none
JSV_FLAGS += -DPrivate.propertyFile=$(PRIVATE_FILE)
JMOL_FLAGS += -DPrivate.propertyFile=$(PRIVATE_FILE)

build:
# JSV
	cd JSpecViewLib && ant clean && ant # "ant clean" actually copies files from Jmol
	cd JSpecView && ant $(JSV_FLAGS) make-application-jar javadoc
# Jmol, part 1
	mkdir -p Jmol/build
	cd Jmol && ant $(JMOL_FLAGS) main
# JSmol
# Note: we *must* supply Eclipse with -data $$PWD otherwise this build will
# interfere with other workspaces and has a high chance of failing.
	cd JSmol && ant -f build_11_fromjmol.xml
	cd JSmol && ant -f build_12_fromjspecview.xml
	eclipse -consoleLog -clean -debug -data $$PWD \
	  -nosplash -application net.sf.j2s.ui.cmdlineApi \
	  -cmd build -path $$PWD/JSmol
	cd JSmol && ant -Djmol.path=../Jmol -f build_13_tojs.xml
# Jmol, part 2
	cd Jmol && ant $(JMOL_FLAGS) all

test:
	cd Jmol && ant $(JMOL_FLAGS) test

clean:
# JSmol
	rm -rf JSmol/bin JSmol/dist/jsmol.zip JSmol/jnlp/JmolApplet*.jar
	rm -rf JSmol/site JSmol/src JSmol/srcjs/js
	rm -rf $$PWD/.metadata # clean eclipse workspace
# Jmol
	mkdir -p Jmol/build
	cd Jmol && ant $(JMOL_FLAGS) clean clean-after-dist
	rm -rf Jmol/build Jmol/appletweb/jsmol.zip Jmol/jars/JSpecView.jar
	rm -rf Jmol/src/org/jmol/translation/translations.tgz Jmol/Jmol.properties
# JSV
	cd JSpecView && ant $(JSV_FLAGS) clean
	rm -rf JSpecView/bin JSpecView/build JSpecView/doc JSpecView/src/org
	cd JSpecViewLib && ant clean
	rm -rf JSpecViewLib/src/javajs JSpecViewLib/src/org

.PHONY: configure build test clean
eof
# this variable doesn't propagate out of this subshell
touch -d@"$lastdate" Makefile

################################################################################
# Create component tarballs
################################################################################

mv_data() {
	mkdir -p "data/$(dirname "$1")"
	mv "$1" "data/$1"
}

# Some parts are very big and not actually used by the build, so split those
# into a separate tarball for anyone that wants to use them for anything.
mv_data Jmol/_documents
mv_data JSmol/xls
mv_data JSmol/data
mv_data JSpecView/data
mv_data JSpecView/extras/CIE_1931.xlsx
mv_data JSpecView/pdf

) # exit subshell, we go back to the original directory

# reproducible output
tar="tar --numeric-owner --owner=1000 --group=1000 --mtime @$(stat -c %Y ${pkgdir}/Makefile) --clamp-mtime"
# Tar it up
echo >&2 "Creating tarball: ${pkgdir}.tar.xz"
$tar -cJ --exclude "$pkgdir/data" -f "${pkgdir}.tar.xz" "$pkgdir"
echo >&2 "Creating tarball: ${pkgdir}-data.tar.xz"
cd "$pkgdir/data" && $tar -cJ -f "../../${pkgdir}-data.tar.xz" "."
